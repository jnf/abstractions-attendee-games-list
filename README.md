# Abstractions Games List

What games do you want to play with Abstractions attendees?

- [Netrunner](https://boardgamegeek.com/boardgame/124742/android-netrunner)
- [Werewolf](https://boardgamegeek.com/boardgame/38159/ultimate-werewolf-ultimate-edition)
- [Vampire](https://boardgamegeek.com/boardgame/180956/one-night-ultimate-vampire)
- [Coup](https://boardgamegeek.com/boardgame/131357/coup)
- [Escape from the Aliens in Outer Space](https://boardgamegeek.com/boardgame/82168/escape-aliens-outer-space)
- [Magic: the Gathering](https://boardgamegeek.com/boardgame/463/magic-gathering)
- [Betrayal at House on the Hill](https://boardgamegeek.com/boardgame/10547/betrayal-house-hill)
- [Pandemic](https://boardgamegeek.com/boardgame/30549/pandemic)
- [Shadow Over Camelot](https://boardgamegeek.com/boardgame/15062/shadows-over-camelot)
